﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            var mon = new String[] { "01_Січень", "02_Лютий", "03_Березень", "04_Квітень", "05_Травень", "06_Червень", "07_Липень", "08_Серпень", "09_Вересень", "10_Жовтень", "11_Листопад", "12_Грудень" };
            Func<int, string> sMonth = delegate (int ii) { return mon[--ii]; };
            Action<string> testDir = delegate (string s) { if (!Directory.Exists(s)) Directory.CreateDirectory(s); };
            Action<string> log = delegate (string s) { Console.WriteLine(s); };
            string doDir = "C:\\PA\\DO";
            string doneDir = "C:\\PA\\DONE";
            testDir("C:\\PA");
            testDir(doDir);
            testDir(doneDir);
            var jpg = Directory.EnumerateFiles(doDir, "IMG_????????_??????*.jpg", SearchOption.AllDirectories).ToList();
            log("CountDCIM=" + jpg.Count);
            jpg.ForEach(x =>
            {
                var nm = x.ToString().Split('\\').Last();
                var yr = nm.Substring(4, 4);
                var mo = nm.Substring(8, 2);
                string s = doneDir + "\\" + yr + "\\" + sMonth(Int32.Parse(mo));
                testDir(s);
                File.Move(x, s + "\\" + nm);
            });

            var prtr = Directory.EnumerateFiles(doDir, "*PORTRAIT*BURST*.jpg", SearchOption.AllDirectories).ToList();
            prtr.ForEach(x =>
            {
                var nm = Path.GetFileName(x);
                var n = nm.Split('_').Where(xx => xx.StartsWith("BURST")).First().Replace("BURST","");
                var yr = n.Substring(0, 4);
                var mo = n.Substring(4, 2);
                string s = doneDir + "\\" + yr + "\\" + sMonth(Int32.Parse(mo));
                testDir(s);
                File.Move(x, s + "\\" + nm);
            });
        
            var vid = Directory.EnumerateFiles(doDir, "VID_????????_??????*.mp4", SearchOption.AllDirectories).ToList();
            vid.ForEach(x =>
            {
                var nm = Path.GetFileName(x);
                var yr = nm.Substring(4, 4);
                var mo = nm.Substring(8, 2);
                string s = doneDir + "\\" + yr + "\\video\\" + sMonth(Int32.Parse(mo));
                testDir(s);
                File.Move(x, s + "\\" + nm);
            });



            var sdcard = Directory.GetDirectories(doDir, "sdcard", SearchOption.AllDirectories).First();
            //testDir(doneDir + @"\CallRecordings");
            Action<string> moveDir = delegate (string s) {
                try { Directory.Move(sdcard + @"\" + s, doneDir + @"\" + s); log(s + " Moved"); }
                catch
(DirectoryNotFoundException e)
                { log("Couldn't find " + s); };
            };
            moveDir("CallrRecordings");
            moveDir("GBInstagram");
            moveDir("Download");
            moveDir("Movies");
            moveDir("Music");
            moveDir("Pictures");
            moveDir("Snapseed");
            moveDir("Telegram");
            moveDir("viber");
            moveDir("VK");
            moveDir("bluetooth");
            moveDir(".vkontakte");
            int i = 1;


        }
    }
}
